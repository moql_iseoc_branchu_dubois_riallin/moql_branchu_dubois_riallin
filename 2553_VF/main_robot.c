#include <msp430g2553.h>
#include <string.h>

#include "typedef.h"
#include "robot_sambot.h"
#include "UART_2553.h"
#include "SPI_2553.h"
#include "ADC.h"



INT32 main(void)
{

    init_board();
    init_SPI();
    init_UART();
    init_moteur();

    __bis_SR_register(LPM0_bits + GIE); /*Enter LPM0, interrupts enabled*/

    __enable_interrupt();

    return 0;

}

/*Fonction d'interruption pour les bus de communication*/
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    UCHAR carac;
    UCHAR consigne;

    /* UART */
    if ((IFG2 & UCA0RXIFG)==1)
    {
        carac= UCA0RXBUF;
        menu_UART(carac);
    }

    /* SPI */
    else if ((IFG2 & UCB0RXIFG)==1)
    {
        while( ((UCB0STAT & UCBUSY)==1) && ((UCB0STAT & UCOE)==0) ){}   /*transmitting/receiving and no error*/
        while((IFG2 & UCB0RXIFG)==0){}
        consigne = UCB0RXBUF;
        menu_SPI(consigne);
    }
    else
    {
	/*LDRA veut un else*/
    }
}

