#include <msp430g2553.h>
#include "typedef.h"
#include "ADC.h"

INT32 flag = 0; /*0=arret; 1=avancer; -1=reculer*/
INT32 result;

/* Initialisation de la carte*/
void init_board(void)
{
    WDTCTL = WDTPW + WDTHOLD; /*Stop WDT*/
    /*clock calibration verification*/

    if((CALBC1_1MHZ==0xFF) || (CALDCO_1MHZ==0xFF))
    {
      __low_power_mode_4();
    }
    else
    {
    /*LDRA veut un else*/
    }

    BCSCTL1 = CALBC1_1MHZ; /*Set DCO*/
    DCOCTL = CALDCO_1MHZ;
}

/*Initialisation des ports pour les moteurs*/
void init_moteur(void){

    P2SEL |= (BIT2 | BIT4);
    P2SEL2 &=~ (BIT2 | BIT4);

    P2DIR |= (BIT2 | BIT4 | BIT1 | BIT5);

    TA1CTL |= (TASSEL_2 | MC_1 | ID_0);

    TA1CCTL1 |= OUTMOD_7;
    TA1CCTL2 |= OUTMOD_7;

    P2OUT |= BIT5; /*sens moteur B*/
    P2OUT &=~ BIT1; /*sens moteur A*/

    TA1CCR0 = 100;
}

/*fonction d'arr�ts des moteurs*/
void arreter(void)
{
    P2OUT &=~ (BIT5 | BIT1);
    TA1CCR1 = 0; /*moteur gauche*/
    TA1CCR2 = 0; /*moteur droit*/
    flag = 0;
}

/*fonction pour faire avancer le robot*/
void avancer(void)
{
    P2OUT &=~ BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = 80; /*moteur gauche*/
    TA1CCR2 = 80; /*moteur droit*/
    flag = 1;
}

/*fonction pour faire reculer le robot*/
void reculer(void)
{
    P2OUT |= BIT1;
    P2OUT &=~ BIT5;
    TA1CCR1 = 80; /*moteur gauche*/
    TA1CCR2 = 80; /*moteur droit*/
    flag = -1;
}

/*fonction pour que le robot tourne vers la gauche*/
void tourner_gauche(void)
{
    P2OUT &=~ BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = 0; /*moteur gauche*/
    TA1CCR2 = 50; /*moteur droit*/
    __delay_cycles(500000);

    /*v�rification si le robot doit repartir en reculant ou en avan�ant*/
    if (flag == 1)
    {
        avancer();
    }
    else if (flag == -1)
    {
        reculer();
    }
    else
    {
        arreter();
    }

}

/*fonction pour que le robot tourne vers la droite*/
void tourner_droite(void)
{
    TA1CCR1 = 50; /*moteur gauche*/
    TA1CCR2 = 0; /*moteur droit*/
    P2OUT &=~ BIT1;
    P2OUT |= BIT5;
    __delay_cycles(500000);

    /*v�rification si le robot doit repartir en reculant ou en avan�ant*/
    if (flag == 1)
    {
        avancer();
    }
    else if (flag == -1)
    {
        reculer();
    }
    else
    {
        arreter();
    }
}
