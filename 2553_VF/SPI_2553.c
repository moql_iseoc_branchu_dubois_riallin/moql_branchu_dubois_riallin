#include <msp430g2553.h>

#include "UART_2553.h"
#include "robot_sambot.h"
#include "typedef.h"


#define SCK         BIT5            /*Serial Clock*/
#define DATA_OUT    BIT6            /*DATA out*/
#define DATA_IN     BIT7            /*DATA in*/

/*Inititialisation du SPI*/
void init_SPI(void)
{
    /* Waste Time, waiting Slave SYNC*/
    __delay_cycles(250);

    /* SOFTWARE RESET - mode configuration*/
    UCB0CTL0 = 0;
    UCB0CTL1 = (0 + (UCSWRST*1) );

    /* clearing IFg /16.4.9/p447/SLAU144j
       set by setting UCSWRST just before*/
    IFG2 &= ~(UCB0TXIFG | UCB0RXIFG);

    UCSYNC = 1 -> Mode synchrone (SPI)*/
    UCB0CTL0 |= ( UCMST | UCMODE_0 | UCSYNC );
    UCB0CTL0 &= ~( UCCKPH | UCCKPL | UCMSB | UC7BIT );
    UCB0CTL1 |= UCSSEL_2;

    UCB0BR0 = 0x0A;     /*divide SMCLK by 10*/
    UCB0BR1 = 0x00;

    /* SPI : Fonctions secondaires
       MISO-1.6 MOSI-1.7 et CLK-1.5
       Ref. SLAS735G p48,49*/
    P1SEL  |= ( SCK | DATA_OUT | DATA_IN);
    P1SEL2 |= ( SCK | DATA_OUT | DATA_IN);

    UCB0CTL1 &= ~UCSWRST;                                /*activation USCI*/
}

/*Reception d'un caract�re via le SPI*/
void Send_char_SPI(UCHAR carac)
{
    while ((UCB0STAT & UCBUSY)==1){}   	/*attend que USCI_SPI soit dispo.*/
    while((IFG2 & UCB0TXIFG)==0){} 	/*p442*/
    UCB0TXBUF = carac;              /* Put character in transmit buffer*/
	/*LDRA veut caster int/unsigned char*/
}

/*Fonction qui d�termine ce que le robot doit faire en fonction des caract�res re�us en SPI*/
void menu_SPI(UCHAR c)
{

    switch(c)
    {

        case 'z' :
            avancer();
            break;

        case 's' :
            reculer();
            break;

        case 'd' :
            tourner_droite();
            break;

        case 'q' :
            tourner_gauche();
            break;

        case 'a' :
            arreter();
            break;

        default :
		/*erreur*/
            break;

    }
}
