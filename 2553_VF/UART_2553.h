#include "typedef.h"

#ifndef UART_2553_H
#define UART_2553_H

void menu_UART(UCHAR c);

void init_UART(void);

void RXdata_UART(UCHAR *c);

void TXdata_UART(UCHAR c );

void Send_STR_UART(const UCHAR *msg);

#endif

