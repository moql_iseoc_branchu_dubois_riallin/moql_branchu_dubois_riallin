/*Inclusion des biblioth�ques*/
#include <msp430.h> 
#include "SPI_2231.h"
#include "typedef.h"
#include "ADC.h"

/*D�finition de variables*/
#define servo BIT2;
#define infrarouge BIT4;
INT32 tampon=0;

/*Initialisation du timer n�cessaire au controle du servomoteur*/
void init_timer(){
    TACTL = TASSEL_2 | MC_1 | ID_0; /* source SMCLK pour TimerA (no 2), mode comptage Up*/
    TACCTL1 |= OUTMOD_7; /* activation mode de sortie n�7*/
    TACCR0 = 12500; /* determine la periode du signal*/
    TA0CTL &= ~TAIFG;/* pour le capteur infrarouge*/
}

/*Initialisation des registres destin�s au controle du servomoteur*/
void init_servo(){
    P1DIR |= servo; /* utilisation de la fonction TA0.1*/
    P1SEL |= servo;
}

/*Initialisation des registres li�s au capteur infrarouge*/
void init_infrarouge(){
    P1DIR &=~ infrarouge;
    P1SEL &=~ infrarouge;
}

/*Fonction retournant l'angle approximatif du servomoteur en fonction d'une valeur d'entree correspondant � la valeur de TACCR1*/
INT32 retourangle(INT32 tampon){
    switch(tampon){
    case 900 :
            return 20;
        break;
    case 1200 :
            return 50;
        break;
    case 1500 :
            return 80;
        break;
    case 1750 :
            return 100;
        break;
    case 2000 :
            return 120;
        break;
    case 2300 :
            return 150;
        break;
    default :
            return 360;
        break;
    }
}

/**
 * Fonction main
 */
INT32 main(void)
{
	/*Appel des fonctions initialisant les parametres necessaires au fonctionnement du syst�me*/
	init_board();
	init_SPI();
    init_timer();/*initialisation du Timer*/
    init_servo();/*initialisation du servomoteur*/
    init_infrarouge();/*initialisation de capteur infrarouge*/

	/*Autorisation des interruptions*/
    __enable_interrupt();

	/*Bouvle infinie servant � faire tourner le servomoteur tant que le robot est allum�*/
    while(1){
        TACCR1 = 900;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 1200;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 1500;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 1750;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 2000;
           tampon = TACCR1;
           __delay_cycles(200000);
        TACCR1 = 2300;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 2000;
           tampon = TACCR1;
           __delay_cycles(200000);
        TACCR1 = 1750;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 1500;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 1200;
        tampon = TACCR1;
        __delay_cycles(200000);
        TACCR1 = 900;
        tampon = TACCR1;
        __delay_cycles(200000);
}
	
	return 0;
}


/*Interruption pour la reception de caractere via le bus SPI*/
#pragma vector=USI_VECTOR
__interrupt void USI(void)
{
    P1OUT^=BIT0;
    while((USICTL1 & USIIFG)==0){}   /* waiting char by USI counter flag*/
    USISRL = 'z';
    USICNT &=~ USI16B;  /* re-load counter & ignore USISRH*/
    USICNT = 0x08;      /* 8 bits count, that re-enable USI for next transfert*/
}

/*#pragma vector = TIMER0_A1_VECTOR
__interrupt void stop_or_turn(void) {
    //stop for obstacle
    int res;
    int angle;
        ADC_init();
       ADC_Demarrer_conversion(4);                 //ADC sur le Pin 4 pour le capteur infrarouge
       res = ADC_Lire_resultat();
       while(res>500){
           P1SEL &= ~BIT0;/* allumage d'une LED pour la v�rification
           P1DIR |= BIT0;
           P1OUT |= BIT0;
           ADC_Demarrer_conversion(4);
           res = ADC_Lire_resultat();
           angle=retourangle(tampon);/* recup�ration de la valeur de l'angle en fonction de TACCR1
       }
       P1OUT &= ~BIT0;
       TA0CTL &= ~TAIFG;
}*/
